package main.Lesson3;

import java.util.Arrays;

public class InsertionSort {


    public static void main(String[] args) {

        int[] numbers = new int[]{1, 55, 31, 4, 45, 100, 4,-4,487,-7,0,4456,4};
        int[] numbersSorted = insertionSort(numbers);
        System.out.println(Arrays.toString(numbersSorted));
    }

    private static int[] insertionSort(int[] numbers) {

        for (int i = 1; i < numbers.length; i++) {
            for (int j = i; j > 0 && numbers[j - 1] > numbers[j]; j--) {

                int tmp = numbers[j - 1];
                numbers[j - 1] = numbers[j];
                numbers[j] = tmp;
            }
        }


        return numbers;
    }


}
