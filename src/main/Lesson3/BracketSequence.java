package main.Lesson3;

//Дана некоторая скобочная последовательность (скобки: “(“, “)” ). Определить,
//является ли она правильной.

import java.util.Stack;

public class BracketSequence {

    public static void main(String[] args) {
        String brackets = "()()(()()(())())";
        System.out.println("Bracket sequence: " + isItCorrectBracketSequence(brackets));

        String brackets2 = "()()(()()(()a)())";
        System.out.println("Bracket sequence: " + isItCorrectBracketSequence(brackets2));


        String brackets3 = "()()(()()(())))())";
        System.out.println("Bracket sequence: " + isItCorrectBracketSequence(brackets3));

        String brackets4 = "(((((((((()()(()()(())))())";
        System.out.println("Bracket sequence: " + isItCorrectBracketSequence(brackets4));
    }

    private static boolean isItCorrectBracketSequence(String brackets) {
        Stack<Integer> stack = new Stack<Integer>();
        char[] brackets_array = brackets.toCharArray();
        for (char s : brackets_array) {
            if (s == '(') {
                stack.push(null);
            } else if (s == ')') {
                if (!stack.empty()) {
                    stack.pop();
                } else {
                    return false;
                }
                // если внутри другой элемент, отличающийся от скобки
            } else {
                return  false;
            }
        }
        if (stack.empty())
            return true;
        else {
            return false;
        }

    }
}
