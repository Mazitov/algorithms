package main.Lesson3;

import java.util.Arrays;

public class CountingSort {

// Мой алгоритм сортирует только положительные целые числа
    public static void main(String[] args) {
        int[] numbers = new int[]{1, 55, 31, 4, 45, 100, 4};
        int[] numbersSorted = radixSort(numbers);
        System.out.println(Arrays.toString(numbersSorted));
    }

    private static int[] radixSort(int[] numbers) {
        int[] array_index = new int[findmax(numbers)];
        int[] sorted_array = new int[numbers.length];
        int cursour = 0;
        for (int i = 0; i < numbers.length; i++){
            array_index[numbers[i]-1] += 1;
        }

        for (int i = 0; i < array_index.length; i++) {
            while (array_index[i] > 0) {
                sorted_array[cursour] = i + 1;
                ++cursour;
                --array_index[i];
            }
        }
        return sorted_array;
    }

    private static int findmax(int[] numbers) {
        int i = numbers[0];
        for (int s : numbers) {
            if (s > i) i = s;
        }
        return i;
    }


}
