package main.Lesson1;
//Сдвинуть массив на k-позиций влево

import java.util.Arrays;

public class Exc6 {
    public static void main(String[] args) {
        int[] array = {1, 2, 3, 4, 5, 6, 7, -8, 9, 10};
        int[] new_array = sdvinutMassiv(array, 7);

        System.out.println(Arrays.toString(new_array));
    }

    private static int[] sdvinutMassiv(int[] massiv, int k) {
        if (k == 0) return massiv;
        int[] new_array = new int[massiv.length];
        new_array[new_array.length - 1] = massiv[0];
        for (int i = 0; i < massiv.length - 1; i++) {
            new_array[i] = massiv[i + 1];
        }
        return sdvinutMassiv(new_array, k - 1);
    }

}
