package main.Lesson1;
//2. . Дан массив целых числе Требуется удалить из него все вхождения
//заданного числа ;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Exc2 {

    public static void main(String[] args) {
        int[] numbers = new int[]{100, -300, 100, 500, 800, 125};
        int[] numersSorted = removeNumber(100, numbers);
        System.out.println(Arrays.toString(numersSorted));
    }

    private static int[] removeNumber(int i, int[] numbers) {
        List list = new ArrayList<Integer>();
        for (int j : numbers) {
            if (j != i) list.add(j);
        }
        int [] numbersSorted = list.stream().mapToInt(y-> (int) y).toArray();
        return numbersSorted;
    }

}
