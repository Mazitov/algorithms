package main.Lesson1;
//; Посчитать количество единиц в двоичной записи числа

public class Exc5 {

    public static void main(String[] args) {
        int a = 233;
        System.out.println(Integer.toBinaryString(a));
        System.out.println(howManyOne(a));
    }

    private static int howManyOne(int a) {
        String b = Integer.toBinaryString(a);
        char[] array = b.toCharArray();
        int number = 0;
        for (char i : array) {
            if (Character.getNumericValue(i) == 1) number++;
        }
        return number;
    }
}
