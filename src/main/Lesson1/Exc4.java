package main.Lesson1;
//Дан массив чисел Требуется найти такое число которое равномерно
//удаленно от минимума и максимума ;

public class Exc4 {

    public static void main(String[] args) {
        int[] massiv = {8, 0, 4, 7, 3, 7, 10, -12, -3, 100};
        int number = findNumber(massiv);
        System.out.println(number);
    }

    private static int findNumber(int[] massiv) {
        int min = massiv[0];
        int max = massiv[0];
        for (int i : massiv) {
            if (i > max) max = i;
            if (i < min) min = i;
        }
        int number = min;
        int how_far = max;
        for (int i : massiv) {
            int j = (max - i) - (i - min);
            if (j < 0) j *= -1;
            if (j < how_far) {
                number = i;
                how_far = j;
            }
        }
        return number;
    }
}
