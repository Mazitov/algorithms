package main.Lesson1;
// Даны три числа Требуется найти среди них медиану

public class Exc1 {

    public static void main(String[] args) {
        int[] numbers = new int[]{100, -300, 100};
        int b = findMedian(numbers);
        System.out.println(b);
    }

    //todo make with if
    private static int findMedian(int[] numbers) {
        sort(numbers);
        return numbers[1];
    }

    private static int[] sort(int[] numbers) {
        for (int i = numbers.length - 1; i > 0; i--) {
            for (int j = 0; j < i; j++) {
                if (numbers[j] > numbers[j + 1]) {
                    int tmp = numbers[j];
                    numbers[j] = numbers[j + 1];
                    numbers[j + 1] = tmp;
                }
            }
        }
        return numbers;
    }


}




