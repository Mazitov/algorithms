package main.Lesson1;

//Дано неотрицательное число Требуется перевернуть его

public class Exc3 {

    public static void main(String[] args) {
        int a = 1234803090;
        int b = revertNumber(a);
        System.out.println(b);
    }

    private static int revertNumber(int a) {
        int temp;
        int number = 0;
        while (a !=0) {
            temp = a % 10;
            number = number * 10 + temp;
            a /= 10;
        }
        return number;
    }


}
