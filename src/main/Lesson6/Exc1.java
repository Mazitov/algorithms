package main.Lesson6;

//1. Реализовать обход в глубину или ширину;


import java.util.HashMap;

public class Exc1 {
    public static void main(String[] args) {
        HashMap<Integer, int[]> graph = new HashMap<Integer, int[]>();
        graph.put(9, new int[]{2, 4});
        graph.put(2, new int[]{1, 3, 5, 7});
        graph.put(3, new int[]{6, 7});
        graph.put(4, new int[]{1, 5, 7});
        graph.put(5, new int[]{2});
        graph.put(6, new int[]{5});
        graph.put(7, new int[]{2});
        graph.put(8, new int[]{});
        graph.put(1, new int[]{9});


        obhod(graph);


    }

    private static void obhod(HashMap<Integer, int[]> graph) {
        // создаём хештаблицу с состоянием кажой ноды
        HashMap<Integer, Boolean> status = new HashMap<Integer, Boolean>();
        int[] nodes = new int[graph.size()];
        int j = 0;
        for (Object i : graph.keySet().toArray()) {
            status.put((int) i, false);
            nodes[j++] = (int) i;
        }

        for (int i : nodes) {
            if (!status.get(i)) {
                System.out.print(i + " ");
                status.put(i, true);
            }
            for (int s : graph.get(i)) {
                if (!status.get(s)) {
                    System.out.print(s + " ");
                    status.put(s, true);
                }
            }
        }




    }


}


