package main.Lesson4;

//2) Дан массив целых чисел. Требуется определить, явлеяется ли он кучей;

public class Exc2_Is_it_a_heap {


    public static void main(String[] args) {
        int[] heap_max = new int[]{20, 11, 15, 6, 9, 7, 8, 1, 3, 0};
        int[] heap_min = new int[]{1, 2, 6, 5, 7, 6, 7, 8, 9, 10};
        int[] not_a_heap = new int[]{1, 55, 6, 5, 7, 6, -7, 8, 9, 10};
        int[] heap_2 = new int[]{1, 1, 1, 1, 1, 1, 1, 1, 1, 1};

        System.out.println(isItHeapForMaximum(heap_max));
        System.out.println(isItHeapForMinimum(heap_max));

        System.out.println(isItHeapForMinimum(heap_min));
        System.out.println(isItHeapForMaximum(heap_min));

        System.out.println(isItHeapForMaximum(not_a_heap));
        System.out.println(isItHeapForMinimum(not_a_heap));

        System.out.println(isItHeapForMaximum(heap_2));
        System.out.println(isItHeapForMinimum(heap_2));

    }

    private static boolean isItHeapForMinimum(int[] heap) {
        for (int i = 1; i < heap.length; i++) {
            if (i % 2 == 0) {
                if (heap[i] < (heap[(i - 1) / 2])) return false;
            } else {
                if (heap[i] < (heap[i / 2])) return false;
            }
        }

        return true;

    }

    private static boolean isItHeapForMaximum(int[] heap) {
        for (int i = 1; i < heap.length; i++) {
            if (i % 2 == 0) {
                if (heap[i] > (heap[i - 1 / 2])) return false;
            } else {
                if (heap[i] > (heap[i / 2])) return false;
            }
        }
        return true;
    }
}
