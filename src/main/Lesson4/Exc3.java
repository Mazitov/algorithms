package main.Lesson4;

//3) Дан массив целых чисел. Требуется найти k-минимальных элементов;

import java.util.Arrays;

public class Exc3 {


    public static void main(String[] args) {
        int[] not_a_heap = new int[]{1, 55, 6, 5, 7, 6, -7, 8, 9, 10};

        int[] min_3_elements = findKmin(not_a_heap, 3);

        System.out.println(Arrays.toString(min_3_elements));
    }

    private static int[] findKmin(int[] not_a_heap, int i) {
        int[] result = new int[i];
        quickSort(not_a_heap, 0, not_a_heap.length - 1);
        for (int j = 0; j < result.length; j++) {
            result[j] = not_a_heap[j];
        }
        return result;
    }


    public static void quickSort(int[] array, int low, int high) {
        if (array.length == 0)
            return;

        if (low >= high)
            return;

        //  опорный элемент
        int middle = low + (high - low) / 2;
        int opora = array[middle];


        int i = low, j = high;
        while (i <= j) {
            while (array[i] < opora) {
                i++;
            }

            while (array[j] > opora) {
                j--;
            }

            if (i <= j) {
                int temp = array[i];
                array[i] = array[j];
                array[j] = temp;
                i++;
                j--;
            }
        }


        if (low < j)
            quickSort(array, low, j);

        if (high > i)
            quickSort(array, i, high);
    }

}
