package main.Lesson4;

//Дана некоторая скобочная последовательность (скобки: “(“, “)” ). Определить,
//является ли она правильной.

import java.util.Stack;

public class BracketSequence_2 {

    public static void main(String[] args) {
        String brackets = "()()(()()(())())";
        System.out.println("Bracket sequence: " + isItCorrectBracketSequence(brackets));

        String brackets2 = "[{({}{}[])[]}]";
        System.out.println("Bracket sequence: " + isItCorrectBracketSequence(brackets2));


        String brackets3 = "()()(()()(())))())";
        System.out.println("Bracket sequence: " + isItCorrectBracketSequence(brackets3));

        String brackets4 = "(((((((((()()(()()(())))())";
        System.out.println("Bracket sequence: " + isItCorrectBracketSequence(brackets4));
    }

    private static boolean isItCorrectBracketSequence(String brackets) {
        Stack<Character> stack = new Stack<Character>();
        char[] brackets_array = brackets.toCharArray();
        for (char s : brackets_array) {
            if (s == '(') {
                stack.push(s);
            } else if (s == '{') {
                stack.push(s);
            } else if (s == '[') {
                stack.push(s);

            } else if (s == ')') {
                if (!stack.empty()) {
                    if (stack.pop().equals('(')) continue;
                    else {
                        return false;
                    }
                } else {
                    return false;
                }
            } else if (s == '}') {
                if (!stack.empty()) {
                    if (stack.pop().equals('{')) continue;
                    else {
                        return false;
                    }
                } else {
                    return false;
                }

            } else if (s == ']') {
                if (!stack.empty()) {
                    if (stack.pop().equals('[')) continue;
                    else {
                        return false;
                    }
                } else {
                    return false;
                }
            }
            // если внутри другой элемент, отличающийся от скобки
            else {
                return false;
            }
        }
        if (stack.empty())
            return true;
        else {
            return false;
        }

    }
}
