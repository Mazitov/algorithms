package main.Lesson4;


// 1) Написать функцию heapify(arr);

import java.util.Arrays;

public class Exc1_heapify {

    public static void main(String[] args) {
        int[] not_a_heap = new int[]{1, 55, 6, 5, 7, 6, -7, 8, 9, 10, 99, -100};
        int[] heap = heapify_min(not_a_heap);
        System.out.println(Arrays.toString(heap));

        int[] heap2 = heapify_max(not_a_heap);
        System.out.println(Arrays.toString(heap2));
    }

    private static int[] heapify_max(int[] array) {
        for (int i = 0; i < array.length; i++) {
            int j = i;
            while (j > 0) {
                if (j % 2 == 0 && (array[j] > array[(j - 1) / 2])) {
                    int temp = array[j];
                    array[j] = array[(j - 1) / 2];
                    array[(j - 1) / 2] = temp;
                    j = (j - 1) / 2;
                } else if (j % 2 != 0 && (array[j] > array[j / 2])) {
                    int temp = array[j];
                    array[j] = array[j / 2];
                    array[j / 2] = temp;
                    j = j / 2;
                } else {
                    j = 0;
                }
            }
        }
        return array;
    }

    private static int[] heapify_min(int[] array) {
        for (int i = 0; i < array.length; i++) {
            int j = i;
            while (j > 0) {
                if (j % 2 == 0 && (array[j] < array[(j - 1) / 2])) {
                    int temp = array[j];
                    array[j] = array[(j - 1) / 2];
                    array[(j - 1) / 2] = temp;
                    j = (j - 1) / 2;
                } else if (j % 2 != 0 && (array[j] < array[j / 2])) {
                    int temp = array[j];
                    array[j] = array[j / 2];
                    array[j / 2] = temp;
                    j = j / 2;
                } else {
                    j = 0;
                }
            }
        }
        return array;
    }

}
