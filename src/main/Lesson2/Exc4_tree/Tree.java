package main.Lesson2.Exc4_tree;

import java.util.ArrayDeque;
import java.util.Stack;

public class Tree {

    int value;
    Tree left;
    Tree right;

    public Tree(int value, Tree left, Tree right) {
        this.value = value;
        this.left = left;
        this.right = right;
    }

    public Tree(int value) {
        this.value = value;
    }

    //Рекурсивный обход дерева и вывод на экран
    public void printTree() {
        System.out.print(this.value + " ");
        if (left != null) {
            left.printTree();
        }

        if (right != null) {
            right.printTree();
        }
    }

    //Обход в глубину
    public void printTreeDeep(Tree node) {
        Stack<Tree> stack = new Stack<Tree>();
        stack.push(node);
        while (!stack.empty()) {
            node = stack.pop();
            System.out.print(node.value + " ");
            if (node.left != null) {
                stack.push(node.left);
            }
            if (node.right != null) {
                stack.push(node.right);
            }

        }
    }

    //Обход в ширину

    public void printTreeWide(Tree node) {
        ArrayDeque<Tree> queue = new ArrayDeque<Tree>();
        queue.add(node);
        while (!queue.isEmpty()) {
            node = queue.remove();
            System.out.print(node.value + " ");
            if (node.left != null) {
                queue.add(node.left);
            }
            if (node.right != null) {
                queue.add(node.right);
            }

        }
    }

}
