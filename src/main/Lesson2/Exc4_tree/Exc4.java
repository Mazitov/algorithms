package main.Lesson2.Exc4_tree;

//4. Реализовать обходы бинарного дерева;

public class Exc4 {


    public static void main(String[] args) {
        Tree root = new Tree(20, new Tree(7, new Tree(4, new Tree(1), new Tree(6)),
                new Tree(9, new Tree(8), new Tree(15))),
                new Tree(35, new Tree(31, new Tree(28), new Tree(32)),
                        new Tree(40, new Tree(38), new Tree(52))));
        root.printTree();
        System.out.println();
        root.printTreeDeep(root);
        System.out.println();
        root.printTreeWide(root);
    }
}
