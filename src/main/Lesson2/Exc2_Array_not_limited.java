package main.Lesson2;

import java.util.Arrays;

//2. Реализовать класс Stack с использованием массива
// если стек переполнен, то увеличиваем его на 50%
// если стек заполнен меньше чем на половину - уменьшаем на 25%

public class Exc2_Array_not_limited {

    int arraysize;
    double[] stack_array;
    int cursour;

    // Конструктор стека
    public Exc2_Array_not_limited() {
        arraysize = 10;
        stack_array = new double[arraysize];
        cursour = -1;
    }

    // пустой ли стек?
    public boolean empty() {
        return cursour == -1;
    }

    // размер стека
    public int size() {
        return stack_array.length;
    }

    // Поместить объект в стек, если объект не помещается в стек, то увеличиваем его
    public boolean push(double number) {
        if (cursour >= arraysize - 1) {
            arraysize = (int) (arraysize * 1.5);
            double[] new_stack = new double[arraysize];
            for (int i = 0; i < stack_array.length; i++) {
                new_stack[i] = stack_array[i];
            }
            stack_array = new_stack;
            stack_array[++cursour] = number;
            return true;
        } else {
            stack_array[++cursour] = number;
            return true;
        }
    }


    // Вытащить объект из стека
    // если стек заполнен меньнше чем на половину, но при этом больше минимальной длины 10, то уменьшить его на 25%
    public double pop() {
        if (empty()) return Double.NaN;
        else {
            double result = stack_array[cursour];
            stack_array[cursour] = 0;
            cursour--;

            if (cursour < stack_array.length / 2 && stack_array.length >= 10) {
                arraysize = (int) (arraysize * 0.75);
                double[] new_stack = new double[arraysize];
                for (int i = 0; i < cursour; i++) {
                    new_stack[i] = stack_array[i];
                }
                stack_array = new_stack;
            }
            return result;
        }
    }

    //Посмотреть верхний элемент без удаления
    //Если элемента нет, то возвращаем NaN, что означает что стек пустой
    public double top() {
        if (!empty()) return stack_array[cursour];
        else {
            return Double.NaN;
        }
    }

    // Посмотреть на весь стек
    public void printStack() {
        System.out.println(Arrays.toString(stack_array));
    }

    //Тест прошёл:
    public static void main(String[] args) {
        Exc2_Array_not_limited stack = new Exc2_Array_not_limited();
        stack.push(5);
        stack.push(4);
        stack.push(3);
        stack.push(2);
        stack.push(1);
        stack.push(5);
        stack.push(4);
        stack.push(3);
        stack.push(2);
        stack.push(1);
        stack.push(5);
        stack.push(4);
        stack.push(3);
        stack.push(2);
        stack.push(1);
        stack.push(5);
        stack.push(4);
        stack.push(3);
        stack.push(2);
        stack.push(1);
        stack.push(5);
        stack.push(4);
        stack.push(3);
        stack.push(2);
        stack.push(1);
        stack.push(5);

        stack.push(4);
        stack.push(3);
        stack.push(2);
        stack.push(1);
        stack.push(5);
        stack.push(4);
        stack.push(3);
        stack.push(2);
        stack.push(1);
        stack.push(5);
        stack.push(4);
        stack.push(3);
        stack.push(2);
        stack.push(1);
        stack.printStack();
        System.out.println(stack.top());
        System.out.println(stack.top());
        System.out.println(stack.top());
        stack.printStack();

        System.out.println(stack.pop());
        System.out.println(stack.pop());
        System.out.println(stack.pop());
        System.out.println(stack.pop());

        System.out.println(stack.pop());
        System.out.println(stack.pop());
        stack.printStack();
        System.out.println(stack.pop());
        System.out.println(stack.pop());
        System.out.println(stack.pop());
        System.out.println(stack.pop());
        System.out.println(stack.pop());
        stack.printStack();
        System.out.println(stack.pop());
        System.out.println(stack.pop());
        System.out.println(stack.pop());
        System.out.println(stack.pop());
        System.out.println(stack.pop());
        System.out.println(stack.pop());
        System.out.println(stack.pop());
        System.out.println(stack.pop());
        System.out.println(stack.pop());
        System.out.println(stack.pop());
        stack.printStack();
        System.out.println(stack.pop());
        System.out.println(stack.pop());
        System.out.println(stack.pop());
        System.out.println(stack.pop());
        System.out.println(stack.pop());
        System.out.println(stack.pop());
        System.out.println(stack.pop());
        System.out.println(stack.pop());
        System.out.println(stack.pop());
        System.out.println(stack.pop());
        System.out.println(stack.pop());
        System.out.println(stack.pop());
        System.out.println(stack.pop());
        System.out.println(stack.pop());
        System.out.println(stack.pop());
        System.out.println(stack.pop());
        System.out.println(stack.pop());
        System.out.println(stack.pop());
        System.out.println(stack.pop());
        System.out.println(stack.pop());
        stack.printStack();

        System.out.println(stack.pop());
        System.out.println(stack.pop());
        System.out.println(stack.pop());
        System.out.println(stack.pop());
        System.out.println(stack.pop());
        System.out.println(stack.pop());
        System.out.println(stack.pop());
        System.out.println(stack.pop());
        System.out.println(stack.pop());
        System.out.println(stack.pop());
        System.out.println(stack.pop());
        stack.push(4);
        stack.push(3);
        stack.push(2);
        stack.push(1);
        stack.push(5);
        stack.push(4);
        stack.push(3);
        System.out.println(stack.pop());
        stack.printStack();
        stack.push(1);
        stack.push(5);
        stack.push(4);
        stack.push(3);
        stack.push(4);
        stack.push(3);
        stack.push(2);
        stack.push(1);
        stack.push(5);
        stack.push(4);
        stack.push(3);
        System.out.println(stack.top());


    }
}
