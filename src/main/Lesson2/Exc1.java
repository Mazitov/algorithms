package main.Lesson2;

//1. Дан упорядоченный массив. Требуется написать функцию бинарного поиска;

public class Exc1 {


    public static void main(String[] args) {
        int[] numbers = new int[]{-457487, -787, -58, -3, -1, 5, 6, 7, 15, 81, 150, 1115, 1545, 15458, 555487, 44878987};
        int a = binarySearch(numbers, 555487);

        System.out.println(a);

    }

    private static int binarySearch(int[] numbers, int i) {
        int left = 0;
        int right = numbers.length - 1;
        int middle = (numbers.length - 1) / 2;
        if (i < numbers[middle]) {
            right = middle;
        } else if (i > numbers[middle]) {
            left = middle;
        } else if (i == numbers[middle]) {
            return middle + 1;
        }

        return binarySearch(numbers, i, left, right);
    }

    private static int binarySearch(int[] numbers, int i, int left, int right) {

        if (right - left == 1) {
            if (numbers[left] == i) return left + 1;
            if (numbers[right] == i) return right + 1;

            return -1;
        }

        int middle = left + ((right - left) / 2);

        if (i < numbers[middle]) {
            right = middle;
        } else if (i > numbers[middle]) {
            left = middle;
        } else if (i == numbers[middle]) {
            return middle + 1;
        }

        return binarySearch(numbers, i, left, right);
    }


}
