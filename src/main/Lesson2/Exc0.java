package main.Lesson2;

//0. Даны два упорядоченных массива. Требуется выполнить слияние этих массивов.
//Результат – упорядоченный массив;

import java.util.Arrays;

public class Exc0 {

    public static void main(String[] args) {
        int[] numbers = new int[]{1, 2, 3, 4, 5, 45};
        int[] numbers2 = new int[]{-3, -1, 5, 6, 7, 15, 81, 150, 1115};

        int[] numbers3 = concatinateArrays(numbers, numbers2);

        System.out.println(Arrays.toString(numbers3));

    }

    private static int[] concatinateArrays(int[] numbers, int[] numbers2) {

        int i_ar1 = 0; //курсор по первому массиву
        int i_ar2 = 0; //курсор по второму массиву

        int[] numbers3 = new int[numbers.length + numbers2.length];
        for (int i = 0; i < numbers3.length; i++) {
            // если первый массив закончился, то добавляем только из второого
            if (i_ar1 >= numbers.length) {
                numbers3[i] = numbers2[i_ar2];
                i_ar2++;
                continue;
            }

            // если второй массив закончился берем только из первого
            if (i_ar2 >= numbers2.length) {
                numbers3[i] = numbers[i_ar1];
                i_ar1++;
                continue;
            }

            //сравниваем элементы и добавляем нужные
            if (numbers[i_ar1] <= numbers2[i_ar2]) {
                numbers3[i] = numbers[i_ar1];
                i_ar1++;
                continue;
            } else {
                numbers3[i] = numbers2[i_ar2];
                i_ar2++;
                continue;
            }
        }
        return numbers3;
    }


}
