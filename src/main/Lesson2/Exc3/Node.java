package main.Lesson2.Exc3;

public class Node {

    int number;
    Node next;


    public Node(int number) {
        this.number = number;
    }

    public Node(int number, Node next) {
        this.number = number;
        next.next=this;
    }


}
