package main.Lesson2.Exc3;


public class List {


    public void printList(Node node) {
        while (node != null) {
            System.out.print(node.number + " ; ");
            node = node.next;
        }
        System.out.println();
    }

    public void revertList(Node node) {
        Node start = node;
        Node node_2 = node.next;
        Queue queue = new Queue();

        while (node != null) {
            queue.addFirst(node);
            node = node.next;
        }

        node = start;
        node.next=null;
        node = node_2;

        while (node != null) {
            node_2 = node.next;
            node.next= queue.poll();
            node = node_2;
        }
    }

}
