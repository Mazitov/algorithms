package main.Lesson2.Exc3;

import java.util.LinkedList;

public class Queue {

    LinkedList<Node> queue;


    public Queue() {
        queue = new LinkedList<Node>();
    }

// добавляем элемент в очередь
    public void addFirst(Node node) {
        queue.add(node);
    }

    public boolean empty() {
        if (queue.size() == 0) return true;
        else {
            return false;
        }
    }

    //возвращает элемент с удалением из очереди
    public Node poll() {
        if (!empty()) {
            return queue.getLast();
        } else {
            return null;
        }
    }


}
