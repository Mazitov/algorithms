package main.Lesson2.Exc3;

//3*. Дан односвязный список. Требуется выполнить переворот списка;

public class Exc3 {


    public static void main(String[] args) {
        Node obj1 = new Node(1);
        Node obj2 = new Node(2, obj1);
        Node obj3 = new Node(3, obj2);
        Node obj4 = new Node(4, obj3);
        Node obj5 = new Node(5, obj4);

        List list = new List();
        list.printList(obj1);

        list.revertList(obj1);
        list.printList(obj1);
        list.printList(obj2);

    }


}
