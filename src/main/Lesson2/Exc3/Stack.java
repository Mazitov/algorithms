package main.Lesson2.Exc3;
//2. Реализовать класс Stack с использованием списка

import java.util.LinkedList;

public class Stack {


    LinkedList<Node> stack;

    public Stack() {
        stack = new LinkedList<Node>();
    }

    public void push(Node node) {
        stack.add(node);
    }

    public Node pop() {
        Node result = stack.getLast();
        stack.removeLast();
        return result;
    }

    public Node top() {
        return stack.getLast();
    }

    public boolean empty() {
        if (stack.size() == 0) return true;
        else {
            return false;
        }
    }

    public int size() {
        return stack.size();
    }

    public void printStack() {
        for (Node i : stack) {
            System.out.print(i.number + "; ");
        }
        System.out.println();
    }




}
