package main.Lesson2;

import java.util.Arrays;

//2. Реализовать класс Stack с использованием массива (конечной длины)
public class Exc2_Array_with_limit {

    int arraysize;
    double[] stack_array;
    int cursour;

    // Конструктор стека с выбранным значением длины
    public Exc2_Array_with_limit(int arraysize) {
        this.arraysize = arraysize;
        stack_array = new double[arraysize];
        cursour = -1;
    }

    // пустой ли стек?
    public boolean empty() {
        return cursour == -1;
    }

    // размер стека
    public int size() {
        return stack_array.length;
    }

    // Поместить объект в стек
    public boolean push(double number) {
        if (cursour == arraysize - 1) return false;
        else {
            stack_array[++cursour] = number;
            return true;
        }
    }


    // Вытащить объект из стека
    public double pop() {
        if (empty()) return Double.NaN;
        double result = stack_array[cursour];
        stack_array[cursour] = 0;
        cursour--;
        return result;
    }

    //Посмотреть верхний элемент без удаления
    //Если элемента нет, то возвращаем NaN, что означает что стек пустой
    public double top() {
        if (!empty()) return stack_array[cursour];
        else {
            return Double.NaN;
        }
    }

    // Посмотреть на весь стек
    public void printStack() {
        System.out.println(Arrays.toString(stack_array));
    }

    public static void main(String[] args) {
        Exc2_Array_with_limit stack = new Exc2_Array_with_limit(10);
        stack.push(5);
        stack.push(4);
        stack.push(3);
        stack.push(2);
        stack.push(1);
        stack.push(5);
        stack.push(4);
        stack.push(3);
        stack.push(2);
        stack.push(1);

        stack.printStack();

        System.out.println(stack.pop());
        System.out.println(stack.pop());
        System.out.println(stack.pop());
        System.out.println(stack.pop());
        System.out.println(stack.pop());
        System.out.println(stack.pop());
        System.out.println(stack.pop());
        System.out.println(stack.pop());
        System.out.println(stack.pop());
        System.out.println(stack.pop());
        System.out.println(stack.pop());
        System.out.println(stack.pop());
        System.out.println(stack.pop());
        System.out.println(stack.pop());
        System.out.println(stack.pop());

        stack.printStack();
        stack.push(1);
        stack.push(5);
        stack.push(4);
        stack.push(3);

        System.out.println(stack.top());


    }


}
