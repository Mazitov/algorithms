package main.Lesson2;
//2. Реализовать класс Stack с использованием списка

import java.util.LinkedList;

public class Exc2_List {


    LinkedList<Integer> stack;

    public Exc2_List() {
        stack = new LinkedList<Integer>();
    }

    public void push(int number) {
        stack.add(number);
    }

    public Integer pop() {
        Integer result = stack.getLast();
        stack.removeLast();
        return result;
    }

    public Integer top() {
        return stack.getLast();
    }

    public boolean empty() {
        if (stack.size() == 0) return true;
        else {
            return false;
        }
    }

    public int size() {
        return stack.size();
    }

    public void printStack() {
        for (Integer i : stack) {
            System.out.print(i + "; ");
        }
        System.out.println();
    }



    //Тест
    public static void main(String[] args) {

        Exc2_List list = new Exc2_List();
        System.out.println(list.empty());
        list.push(1);
        list.push(2);
        list.push(3);
        list.push(4);
        list.push(5);
        list.push(6);
        list.push(7);
        list.push(8);
        list.printStack();

        list.pop();
        list.pop();
        list.pop();
        list.printStack();

        System.out.println(list.size());
        System.out.println(list.top());


    }


}
