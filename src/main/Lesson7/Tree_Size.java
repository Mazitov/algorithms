package main.Lesson7;


public class Tree_Size {

    int value;
    Tree_Size left;
    Tree_Size right;

    public static void main(String[] args) {
        Tree_Size root = new Tree_Size(20, new Tree_Size(7, new Tree_Size(4, new Tree_Size(1), new Tree_Size(6)),
                new Tree_Size(9, new Tree_Size(8), new Tree_Size(15))),
                new Tree_Size(35, new Tree_Size(31, new Tree_Size(28), new Tree_Size(32)),
                        new Tree_Size(40, new Tree_Size(38), new Tree_Size(52))));

        System.out.println(whatIsTheTreeSize(root));
    }


    public Tree_Size(int value, Tree_Size left, Tree_Size right) {
        this.value = value;
        this.left = left;
        this.right = right;
    }

    public Tree_Size(int value) {
        this.value = value;
    }

    public static int whatIsTheTreeSize(Tree_Size node) {
        if (node == null) {
            return 0;
        } else {
            return 1 +
                    Math.max(whatIsTheTreeSize(node.left),
                            whatIsTheTreeSize(node.right));
        }
    }

}


