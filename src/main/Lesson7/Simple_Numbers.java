package main.Lesson7;

import java.util.LinkedList;

//12. Вывести первые N простых чисел.

public class Simple_Numbers {

    public static void main(String[] args) {
        int n = 1000;
        LinkedList<Integer> simple_numbers = generateSimpleNumbers(n);
        System.out.println(simple_numbers);
    }

    private static LinkedList<Integer> generateSimpleNumbers(int s) {
        LinkedList<Integer> numbers = new LinkedList<Integer>();

        loop:
        for (int i = 2; numbers.size() < s; i++) {
            if (!numbers.isEmpty()) {
                //проверяем если число не делиться на половину простых чисел в листе, то добавляем его в этот лист
                for (int h = 0; h <= numbers.size() / 2; ++h) {
                    if (i % numbers.get(h) == 0) {
                        continue loop;
                    }
                }
                numbers.add(i);
            } else {
                numbers.add(2);
            }
        }
        return numbers;
    }
}