package main.Lesson5;

import java.util.Hashtable;

public class Symmetrical_dots {

    public static void main(String[] args) {

        int[][] array_dots = new int[][]{{8, 9}, {7, -3}, {4, 3}, {12, 3}, {6, -4}, {10, 4}, {7, 6}, {9, 6}, {8, 9}, {10, 7}, {6, 7}};
        int[][] array_dots2 = new int[][]{{4, 3}, {12, 3}, {8, 9}};
        int[][] array_dots3 = new int[][]{{8, 9}, {7, -3}, {4, 3}, {10, 3}, {6, 4}, {10, -4}, {7, 6}, {9, 6}, {8, 9}, {10, 7}, {6, 7}};
        int[][] array_dots4 = new int[][]{{0, 0}, {0, 0}, {0, 0}};
        int[][] array_dots5 = new int[][]{{0, 2}, {1, 1}, {1, 1}, {1, 1}, {2, 2},  {2, 2}};

        System.out.println(isDotsSymmetrical(array_dots));
        System.out.println(isDotsSymmetrical(array_dots2));
        System.out.println(isDotsSymmetrical(array_dots3));
        System.out.println(isDotsSymmetrical(array_dots4));
        System.out.println(isDotsSymmetrical(array_dots5));
    }

    private static boolean isDotsSymmetrical(int[][] array_dots) {
        // ищем максимум и минимум x
        int min = array_dots[0][0];
        int max = array_dots[0][0];
        for (int[] i : array_dots) {
            if (i[0] > max) max = i[0];
            if (i[0] < min) min = i[0];
        }

        int sym_line = (max + min) / 2;
        Hashtable<Integer, Integer> left = new Hashtable<Integer, Integer>();
        Hashtable<Integer, Integer> right = new Hashtable<Integer, Integer>();

        for (int[] i : array_dots) {
            //i[0] - sym_line == 0  игнорируем, так как точка лежит на оси симметрии
             if (i[0] - sym_line > 0) {
                right.put(i[0] - sym_line, i[1]);
            } else if (i[0] - sym_line < 0) {
                left.put(sym_line - i[0], i[1]);
            } ;
        }
        System.out.println("Правая хештаблица " + left);
        System.out.println("Левая хештаблица " + right);
        return left.equals(right);
    }


}
